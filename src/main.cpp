#include <Arduino.h>
#include <FastLED.h>

void buildTree(CRGB leds[]) {
  for(int i = 0; i <= 4; i++) {
    leds[i] = CRGB::Chocolate;
  }
  for(int i = 5; i <= 28; i++) {
    leds[i] = CRGB::DarkGreen;
  }
  for(int i = 29; i <= 49; i++) {
    long ranLightNumber = random(0, 3);
    switch(ranLightNumber) {
      case 0:
        leds[i] = CRGB::Blue;
        break;
      case 1:
        leds[i] = CRGB::Red;
        break;
      case 2:
        leds[i] = CRGB::Yellow;
        break;
      default:
        leds[i] = CRGB::White;
    }
    // leds[i] = CRGB::Yellow;
    // if(i % 2 == 0) {
    //   leds[i] = CRGB::Red;
    // }
  }
  for(int i = 36; i <= 39; i++) {
    leds[i] = CRGB::DarkRed;
  }
  FastLED.delay(1000);
}

void addStar(CRGB leds[]) {
  for(int i = 50; i <= 75; i++) {
    leds[i] = CRGB::Yellow;
  }
  FastLED.delay(1000);
}

#define LED_PIN 5
#define NUM_LEDS 135
#define BRIGHTNESS 64
#define LED_TYPE WS2811
#define COLOR_ORDER RGB

CRGB leds[NUM_LEDS];

#define UPDATES_PER_SECOND 100

void setup() {
  Serial.begin(9600);
  delay(3000);
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(BRIGHTNESS);
  randomSeed(analogRead(0));
}

void loop() {
  buildTree(leds);
  addStar(leds); 
  FastLED.show();
  FastLED.delay(3000 / UPDATES_PER_SECOND);
  // put your main code here, to run repeatedly:
}